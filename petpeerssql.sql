create database assignment;
use assignment;

CREATE TABLE userregistration(
  id INT(5) NOT NULL AUTO_INCREMENT,
  username varchar(255) NOT NULL,
  password varchar(255) DEFAULT NULL,
  PRIMARY KEY (id));
	 select *from userregistration;


 


 
 
CREATE TABLE pets (
	 id INT(5) NOT NULL AUTO_INCREMENT,
	 petname VARCHAR(55) NOT NULL, 
	 age INT(2),
     place VARCHAR(55),
	 ownerid INT(5) NULL,
	 FOREIGN KEY (ownerid)
        REFERENCES userregistration(id)
        ON DELETE CASCADE,
	 PRIMARY KEY (id)
);

INSERT INTO pets VALUES(0,'Jicko',4,'CH', NULL);
INSERT INTO pets VALUES(0,'Lola',2,'DL', NULL);
INSERT INTO pets VALUES(0,'Chacko',3,'BL', NULL);
INSERT INTO pets VALUES(0,'Chikku',4,'VJ', 1);
INSERT INTO pets VALUES(0,'Buddy',4,'VJ', 2);
INSERT INTO pets VALUES(0,'Tinku',4,'VJ', 2);
INSERT INTO pets VALUES(0,'Dolly',2,'VZG');
update pets set ownerid=3 where id=1;
update pets set ownerid=3 where id=5;

select *from pets;
select * from pets where ownerid=3;

