SELECT firstname
	FROM customer_date cm INNER JOIN master2 am
		ON cm.customer_number=am.customer_number
			GROUP BY firstname having count(firstname)>=2 
				order by firstname; 