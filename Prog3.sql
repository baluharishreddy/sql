use assignment;
CREATE TABLE customer_date (
  acc_no int(11) NOT NULL,
  customer_number int(11) NOT NULL,
  firstname varchar(255) NOT NULL,
  entrydate date NOT NULL
);
insert into customer_date values (1212121212,5,'Balu','1997/07/18');
insert into customer_date values (1323232323,2,'Ravi','1999/08/23');
insert into customer_date values (1546546464,7,'Aravind','1995/12/15');
insert into customer_date values (1646546464,9,'dfdf','1998/09/10');
select * from customer_date;

CREATE TABLE master2 (
  acc_no int(11) NOT NULL,
  customer_number int(11) NOT NULL,
  name varchar(255) NOT NULL,
  entrydate varchar(255) NOT NULL
);
insert into master2 values (1545484544,5,'erer', '1997/07/02');
insert into master2 values (1266452124,2,'cbxc', '1997/07/22');
insert into master2 values (1762223212,7,'dgdgdg','1997/07/17');
insert into master2 values (1862223212,8,'fgfggf','1997/07/08');
select * from master2;

SELECT  m.customer_number, firstname, m.acc_no
  FROM customer_date cm INNER JOIN master2 m
	ON cm.customer_number=m.customer_number
		WHERE extract(day from m.entrydate)>15
			ORDER BY cm.customer_number;	