create database sample;

show databases;

use sample;

show tables;

CREATE TABLE employee (
  sap_id int(11) NOT NULL,
  employee_name varchar(255) DEFAULT NULL,
  employment_type varchar(255) DEFAULT NULL,
  gender varchar(255) DEFAULT NULL,
  monthly_expenditure double DEFAULT NULL
);

DESCRIBE employee;

insert into employee (sap_id,employee_name,employment_type,gender, monthly_expenditure) values (123,'name1','emp type','Male', 1000);

insert into employee (sap_id,employee_name,employment_type) values (234,'name1','emp type');

SELECT * FROM employee;

truncate table employee;

drop table employee;

set sql_safe_updates=0;

update employee set gender = 'Male' where gender is null;

update employee set employee_name = 'name2' where sap_id=234;

update employee set age = age +1;

delete from employee;

delete from employee where sap_id = 123;


select * from employee;

select * from employee order by sap_id asc;

select * from employee where sap_id is null;

select count(*) from employee;

select distinct employee_name from employee;

select sap_id from employee where gender = 'Male';
