use assignment;
CREATE TABLE loan_details (
  acc_no int(11) NOT NULL,
  customer_number int(11) NOT NULL
);
insert into loan_details values (1212121212,5);
insert into loan_details values (1323232323,2);
insert into loan_details values (0312121212,4);
insert into loan_details values (0232323232,3);
insert into loan_details values (1546546464,7);
insert into loan_details values (1546546464,7);
insert into loan_details values (1446546464,20);
truncate table loan_details;
select *from loan_details;
SELECT count(ld.customer_number) count
  FROM customer_date cm INNER JOIN loan_details ld
	ON  cm.customer_number=ld.customer_number
		WHERE cm.customer_number NOT IN ( SELECT customer_number FROM master2);