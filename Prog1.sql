use assignment;
CREATE TABLE customer (
  acc_no int(11) NOT NULL,
  customer_number int(11) NOT NULL,
  firstname varchar(255) NOT NULL,
  lastname varchar(255) DEFAULT NULL,
  date varchar(255) NOT NULL
);
insert into customer values (1212121212,5,'Balu','Reddy', '18/07/1997');
insert into customer values (1323232323,2,'Ravi','Prasad', '19/07/1999');
insert into customer values (1546546464,7,'Aravind','Lankapu', '15/07/1995');
insert into customer values (1646546464,9,'dfdf','jhjhg', '15/07/1995');
select * from customer order by customer_number ;

CREATE TABLE master (
  acc_no int(11) NOT NULL,
  customer_number int(11) NOT NULL,
  name varchar(255) NOT NULL,
  entrydate varchar(255) NOT NULL
);
insert into master values (1545484544,5,'erer', '18/07/1997');
insert into master values (1266452124,2,'cbxc', '19/07/1999');
insert into master values (1762223212,7,'dgdgdg','15/07/1995');
insert into master values (1862223212,8,'fgfggf','15/07/1995');
select * from master;

select * 
	FROM customer cm INNER JOIN master m
		ON cm.customer_number=m.customer_number
			ORDER BY m.acc_no;
	