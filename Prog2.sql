use assignment;
CREATE TABLE customer_city (
  acc_no int(11) NOT NULL,
  customer_number int(11) NOT NULL,
  firstname varchar(255) NOT NULL,
  lastname varchar(255) DEFAULT NULL,
  city varchar(255) NOT NULL
);
insert into customer_city values (1212121212,5,'Balu','Reddy', 'Chennai');
insert into customer_city values (1323232323,2,'Ravi','Prasad', 'Vjd');
insert into customer_city values (1546546464,7,'Aravind','Lankapu', 'HYD');
insert into customer_city values (1646546464,9,'dfdf','jhjhg', 'Chennai');
insert into customer_city values (1446546464,9,'sdfs','fghugj', 'Chennai');

select * from customer_city;
SELECT count(acc_no) as Cust_Count
  FROM customer_city
	WHERE city='Chennai'